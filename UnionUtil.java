class UnionUtil{
        Map<Integer, Integer> eleLeaderMap = new HashMap<>();
        int unionCount = 0;

        public int getUnionCount(){
            return unionCount;
        }

        public void addIfNotExist(Integer ele){
            if(!eleLeaderMap.containsKey(ele)){
                eleLeaderMap.put(ele, ele);
                unionCount++;
            }
        }

        public Integer getLeader(Integer ele){
            if(ele != null){
                Integer leader = eleLeaderMap.get(ele);
                if(ele.equals(leader)){
                    return ele;
                }
                else{
                    Integer finalLeader = getLeader(leader);
                    eleLeaderMap.put(ele, finalLeader);
                    return finalLeader;
                }
            }
            return null;
        }

        public boolean union(Integer eleA, Integer eleB){
            addIfNotExist(eleA);
            addIfNotExist(eleB);
            Integer eleALeader = getLeader(eleA), eleBLeader = getLeader(eleB);
            if(!eleALeader.equals(eleBLeader)){
                eleLeaderMap.put(eleALeader, eleBLeader);
                unionCount--;
                return true;
            }
            return false;
        }
    }