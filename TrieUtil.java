class TrieUtil {
    class trieNode{
        boolean isEnd;
        trieNode[] charTable;
        trieNode(){
            isEnd=false;
            charTable=new trieNode[255];
        }
    }

    trieNode rootNode;
    /** Initialize your data structure here. */
    public TrieUtil() {
        rootNode=new trieNode();
    }

    /** Inserts a word into the trie. */
    public void insert(String word) {
        trieNode tmpNode=rootNode;
        for (int i = 0; i < word.length(); i++) {
            int charLoc=word.charAt(i);
            if(tmpNode.charTable[charLoc]==null){
                tmpNode.charTable[charLoc]=new trieNode();
            }
            tmpNode=tmpNode.charTable[charLoc];
        }
        tmpNode.isEnd=true;
    }

    public trieNode travelFind(String str){
        trieNode tmpNode=rootNode;
        for (int i = 0; i < str.length(); i++) {
            int charLoc=str.charAt(i);
            if(tmpNode.charTable[charLoc]==null){
                return null;
            }
            tmpNode=tmpNode.charTable[charLoc];
        }
        return tmpNode;
    }

    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        trieNode resNode=travelFind(word);
        return resNode!=null&&resNode.isEnd;
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        trieNode resNode=travelFind(prefix);
        return resNode!=null;
    }
}